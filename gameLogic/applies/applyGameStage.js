import { GAME_STAGES } from '../../init/constants.js';
import { lose } from '../sideEffects/lose.js';
import { pause } from '../sideEffects/pause.js';
import { restart } from '../sideEffects/restart.js';

// In this function we check the current game stage
// Which was set by the other function
// And we are making anything depending on that stage
const applyGameStage = (currentGameStage, startInterval, newState) => {
    switch (currentGameStage) {
        case GAME_STAGES.LOSED:
            lose(startInterval);

            break;

        case GAME_STAGES.PAUSED:
            pause(newState);

            break;

        case GAME_STAGES.RESTARTING:
            restart();

            break;

        default:
            break;
    }
};

export { applyGameStage };
