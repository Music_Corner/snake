import { GAME_DEFAULT_STAGE } from "../../init/constants.js";

// ...
const pause = (newState) => {
    alert('Paused');
    newState.currentGameStage = GAME_DEFAULT_STAGE;
};

export { pause };
