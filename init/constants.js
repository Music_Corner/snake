// Constants which are used in the almos every module of app
export const CELL = document.getElementById('canvas').getAttribute('width') / 20;
export const SPEED = 300;
export const SNAKE_ARRAY = [
    {
        x: 5 * CELL,
        y: 5 * CELL,
    },

    {
        x: 4 * CELL,
        y: 5 * CELL,
    },

    {
        x: 3 * CELL,
        y: 5 * CELL,
    },
];
export const DIRECTIONS = {
    RIGHT: 'RIGHT',
    LEFT: 'LEFT',
    UP: 'UP',
    DOWN: 'DOWN',
};
export const DEFAULT_DIRECTION = 'RIGHT';
export const GAME_STAGES = {
    PAUSED: 'PAUSED',
    LOSED: 'LOSED',
    RESTARTING: 'RESTARTING',
};
export const GAME_DEFAULT_STAGE = 'STARTED';
export const APPLE_STAGES = {
    EATEN: 'EATEN',
    NORMAL: 'NORMAL',
};

let bestScore = 0;

if (localStorage.getItem('bestScore')) {
    bestScore = localStorage.getItem('bestScore');
}

export const SCORES = {
    BEST: bestScore,
    CURRENT: 0,
};
