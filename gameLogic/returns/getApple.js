import { APPLE_STAGES, CELL } from '../../init/constants.js';

// Probably the most interesting function (at least for me) in this app

const getApple = (snakeArray) => {

    // Getting two random integer numbers and return potentially apple
    const generateRandomAxis = () => {
        const posX = Math.round(Math.random() * (9 - 1)) * CELL;
        const posY = Math.round(Math.random() * (9 - 1)) * CELL;
        const randomApple = {
            axis: {
                x: posX,
                y: posY,
            },
            state: APPLE_STAGES.NORMAL,
        };

        return randomApple;
    };

    // Check if the pair of numbers from previous function matches the pair of
    // Any axis of snake array
    // Depending on result of checking return false or apple object
    const checkSnakeMatchesRandomAxis = (snakeArray) => {
        const randomApple = generateRandomAxis();

        for (let i = 0; i < snakeArray.length; i += 1) {
            if (snakeArray[i].x === randomApple.axis.x && snakeArray[i].y === randomApple.axis.y) {
                return false;
            }

            if (i === snakeArray.length - 1) {
                return randomApple;
            }

            continue;
        }
    };

    // Then we check if result was new apple object
    // If it does then we return it
    // In the other case we call check function again until it won't return apple object
    let newApple = checkSnakeMatchesRandomAxis(snakeArray);
    while (newApple === false) {
        newApple = checkSnakeMatchesRandomAxis(snakeArray);
    }

    return newApple;
};

export { getApple };
