import { APPLE_STAGES } from "../../init/constants.js";

const returnNewScores = (scores, appleState) => {
    const newScores = Object.assign({}, scores);

    // Setting best score item to local storage
    localStorage.setItem('bestScore', newScores.best);

    // Check if there any value of best score and set the best score value
    if (localStorage.getItem('bestScore')) {
        newScores.best = localStorage.getItem('bestScore');
    }

    // Check if apple stage equals eaten
    // If it has increase current scores
    // Also need to check if the best score value is bigger the current
    //  If it isn't - make it the same value as current one
    if (appleState === APPLE_STAGES.EATEN) {
        newScores.current += 1;
        if (newScores.current >= newScores.best) {
            newScores.best = newScores.current;
        }

        return newScores;
    }

    return newScores;
};

export { returnNewScores };
