import { DIRECTIONS } from "../../init/constants.js";

// Check what key was pressed and directly changing the initial state
const changeSnakeDirection = (e, newState) => {
    switch (e.keyCode) {
        case 38:
            if (newState.currentDirection !== DIRECTIONS.DOWN) {
                newState.currentDirection = DIRECTIONS.UP;
            }
            break;

        case 37:
            if (newState.currentDirection !== DIRECTIONS.RIGHT) {
                newState.currentDirection = DIRECTIONS.LEFT;
            }

            break;

        case 39:
            if (newState.currentDirection !== DIRECTIONS.LEFT) {
                newState.currentDirection = DIRECTIONS.RIGHT;
            }

            break;

        case 40:
            if (newState.currentDirection !== DIRECTIONS.UP) {
                newState.currentDirection = DIRECTIONS.DOWN;
            }

            break;

        default: break;
    }
};

export { changeSnakeDirection };
