import { GAME_STAGES } from '../../init/constants.js';

// Checking of restart was clicked
// Depending on check restart game
const setRestartGameStage = (e, newState) => {
    if (e.target === document.getElementById('restart-game')) {
        newState.currentGameStage = GAME_STAGES.RESTARTING;
    }
};

export { setRestartGameStage };
