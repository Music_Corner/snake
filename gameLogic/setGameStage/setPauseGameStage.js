import { GAME_DEFAULT_STAGE, GAME_STAGES } from '../../init/constants.js';

// Check if the "P" was clicked
// Make alert if it was
const setPauseGameStage = (e, newState) => {
    if (newState.currentGameStage !== GAME_STAGES.LOSED) {
        if (e.keyCode === 80) {
            if (newState.currentGameStage === GAME_DEFAULT_STAGE) {
                newState.currentGameStage = GAME_STAGES.PAUSED;
            }
        } else {
            newState.currentGameStage = GAME_DEFAULT_STAGE;
        }
    }
};

export { setPauseGameStage };
