import { GAME_STAGES } from "../../init/constants.js";

// Check if snake axis matches itself (snake has eaten itself)
// Check if snake first axis hits the board
// Depending on result returning this result
const checkSnakeForDeath = (newSnakeArray, currentGameStage) => {
    const canvas = document.getElementById('canvas');
    const width = canvas.getAttribute('width');
    const height = canvas.getAttribute('height');
    if (newSnakeArray[0].x >= width || newSnakeArray[0].y >= height) {
        return GAME_STAGES.LOSED;
    }

    if (newSnakeArray[0].x < 0 || newSnakeArray[0].y < 0) {
        return GAME_STAGES.LOSED;
    }

    for (let i = 1; i < newSnakeArray.length; i += 1) {
        if (newSnakeArray[0].x === newSnakeArray[i].x && newSnakeArray[0].y === newSnakeArray[i].y) {
            return GAME_STAGES.LOSED;
        }
    }

    return currentGameStage;
};

export { checkSnakeForDeath };
