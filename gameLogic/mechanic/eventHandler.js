import { changeSnakeDirection } from './changeSnakeDirection.js';
import { setPauseGameStage } from '../setGameStage/setPauseGameStage.js';
import { setRestartGameStage } from '../setGameStage/setRestartGameStage.js'

import { applyGameStage } from '../../gameLogic/applies/applyGameStage.js';

// Function which is including to event listener
const eventHandler = (e, newState, startInterval) => {
    changeSnakeDirection(e, newState);
    setPauseGameStage(e, newState);
    setRestartGameStage(e, newState);

    applyGameStage(newState.currentGameStage, startInterval, newState);
};

export { eventHandler };
