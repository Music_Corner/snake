import { CELL } from '../../init/constants.js';

const drawNewState = (newSnakeArray, newApple, scores) => {
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    const currentScoreBoard = document.getElementsByClassName('scores-counter');
    const bestScoreBoard = document.getElementsByClassName('best-counter');

    // Clear the board
    ctx.clearRect(0, 0, canvas.getAttribute('width'), canvas.getAttribute('height'));

    // Draw new snake based on new snake array
    for (let i = 0; i < newSnakeArray.length; i += 1) {
        ctx.fillStyle = (i === 0) ? 'red' : '#ffffffd1';
        ctx.fillRect(newSnakeArray[i].x, newSnakeArray[i].y, CELL, CELL);

        ctx.strokeStyle = 'black';
        ctx.strokeRect(newSnakeArray[i].x, newSnakeArray[i].y, CELL, CELL);
    }

    // Drawing a new score value
    for (let i = 0; i < currentScoreBoard.length; i += 1) {
        currentScoreBoard[i].innerHTML = scores.current;
        bestScoreBoard[i].innerHTML = scores.best;
    }

    // Draw new apple
    ctx.fillStyle = 'green';
    ctx.fillRect(newApple.x, newApple.y, CELL, CELL);
};

export { drawNewState };
