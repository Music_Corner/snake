import { APPLE_STAGES, CELL } from '../../init/constants.js';
import { getApple } from '../returns/getApple.js';

// Here we need to check if the axis of first value of snake array matches
// axis of apple
// Depending on result we return new object of state
const checkAppleForEat = (newSnakeArray, newApple) => {
    if (newSnakeArray[0].x === newApple.axis.x && newSnakeArray[0].y === newApple.axis.y) {
        const justGeneratedApple = getApple(newSnakeArray, CELL);
        justGeneratedApple.state = APPLE_STAGES.EATEN;
        return justGeneratedApple;
    }

    return Object.assign({}, newApple, {
        state: APPLE_STAGES.NORMAL,
    });
};

export { checkAppleForEat };
