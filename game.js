import { returnNewSnakeArray } from './gameLogic/returns/returnNewSnakeArray.js';
import { getApple } from './gameLogic/returns/getApple.js';
import { drawNewState } from './gameLogic/applies/drawNewState.js';
import { checkSnakeForDeath } from './gameLogic/check/checkSnakeForDeath.js';
import { eventHandler } from './gameLogic/mechanic/eventHandler.js';
import { checkAppleForEat } from './gameLogic/check/checkAppleForEat.js';
import { applyGameStage } from './gameLogic/applies/applyGameStage.js';
import { returnNewScores } from './gameLogic/returns/returnNewScores.js';

import { SPEED, SNAKE_ARRAY, DEFAULT_DIRECTION, GAME_DEFAULT_STAGE, SCORES } from './init/constants.js';

const game = () => {
    // Set default state for the first step
    let startInterval;
    const newApple = getApple(SNAKE_ARRAY);
    const newState = {
        snakeArray: SNAKE_ARRAY,
        currentDirection: DEFAULT_DIRECTION,
        apple: newApple,
        currentGameStage: GAME_DEFAULT_STAGE,
        scores: {
            current: SCORES.CURRENT,
            best: SCORES.BEST,
        },
    };
    const startGame = () => {
        // This function making a one step:
        // Creating new apple (by checking has been apple just eaten or not)
        newState.apple = checkAppleForEat(newState.snakeArray, newState.apple);

        // If apple was just eaten the score function will return a new score value
        newState.scores = returnNewScores(newState.scores, newState.apple.state);

        // This makes snake to move and if there's a fact that apple has been eaten
        // adding a new value to the snake array dependent on the previous direction of snake
        newState.snakeArray = returnNewSnakeArray(newState.snakeArray, newState.currentDirection,
            newState.apple.state);

        // Checking if the snake is dead
        // Also this function checks is the game paused or restarting
        newState.currentGameStage = checkSnakeForDeath(newState.snakeArray,
            newState.currentGameStage);

        // After changing the game stage we have to apply them
        // (because some of changes are able to be made with events, so we aren't able
        // to return new state value in that case)
        applyGameStage(newState.currentGameStage, startInterval, newState);

        // Finally drawing a new state on canvas board
        drawNewState(newState.snakeArray, newState.apple.axis, newState.scores);
    };

    // Making a steps in interval
    startInterval = setInterval(startGame, SPEED);

    // Listener of key down (change direction and pause)
    document.addEventListener('keydown', (e) => {
        eventHandler(e, newState, startInterval);
    });

    // Listener of mouse clicking (restart the game)
    document.addEventListener('click', (e) => {
        eventHandler(e, newState, startInterval);
    });
};

game();
