// Stop game
// Show lose screen
const lose = (startInterval) => {
    clearInterval(startInterval);
    document.getElementById('lose-overlay').style.display = 'flex';
};

export { lose };
