import { CELL, DIRECTIONS, APPLE_STAGES } from '../../init/constants.js';

const returnNewSnakeArray = (snakeArray, currentDirection, appleState) => {
    const makeNewSnakeState = (snakeArray, currentDirection) => {
        const state = {};
        const snakeX = snakeArray[0].x;
        const snakeY = snakeArray[0].y;
        let newHead;

        // Check current direction of snake move
        // Depending on that direction make new head axis
        // Also check if snake has just eaten an apple
        // If it has - add a new value of snake cell
        switch (currentDirection) {
            case DIRECTIONS.RIGHT:
                if (appleState === APPLE_STAGES.EATEN) {
                    snakeArray.push({
                        x: snakeArray[snakeArray.length - 1].x + CELL,
                        y: snakeArray[snakeArray.length - 1].y,
                    });
                }

                newHead = {
                    x: snakeX + CELL,
                    y: snakeY,
                };

                Object.assign(state, {
                    newHead: newHead,
                    snakeArray: snakeArray,
                });

                console.log(state);

                return state;

            case DIRECTIONS.LEFT:
                if (appleState === APPLE_STAGES.EATEN) {
                    snakeArray.push({
                        x: snakeArray[snakeArray.length - 1].x - CELL,
                        y: snakeArray[snakeArray.length - 1].y,
                    });
                }

                newHead = {
                    x: snakeX - CELL,
                    y: snakeY,
                };

                Object.assign(state, {
                    newHead: newHead,
                    snakeArray: snakeArray,
                });

                return state;

            case DIRECTIONS.UP:
                if (appleState === APPLE_STAGES.EATEN) {
                    snakeArray.push({
                        x: snakeArray[snakeArray.length - 1].x,
                        y: snakeArray[snakeArray.length - 1].y - CELL,
                    });
                }

                newHead = {
                    x: snakeX,
                    y: snakeY - CELL,
                };

                Object.assign(state, {
                    newHead: newHead,
                    snakeArray: snakeArray,
                });

                return state;

            case DIRECTIONS.DOWN:
                if (appleState === APPLE_STAGES.EATEN) {
                    snakeArray.push({
                        x: snakeArray[snakeArray.length - 1].x,
                        y: snakeArray[snakeArray.length - 1].y + CELL,
                    });
                }
                
                newHead = {
                    x: snakeX,
                    y: snakeY + CELL,
                };

                Object.assign(state, {
                    newHead: newHead,
                    snakeArray: snakeArray,
                });

                return state;

            default: return state;
        }
    };

    // In this function we delete the last item of snake array
    // Then add the new first item which is equals to new head from prev function
    const moveSnake = () => {
        const state = makeNewSnakeState(snakeArray, currentDirection);
        const newSnakeArray = state.snakeArray;

        newSnakeArray.pop();
        newSnakeArray.unshift(state.newHead);

        return newSnakeArray;
    };

    return moveSnake();
};

export { returnNewSnakeArray };
